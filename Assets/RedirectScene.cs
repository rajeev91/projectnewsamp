﻿using UnityEngine;
using System.Collections;

public class RedirectScene : MonoBehaviour
{

    #region PUBLIC_MEMBERS
    public float loadingDelay = 1.0F;
    #endregion //PUBLIC_MEMBERS


    #region MONOBEHAVIOUR_METHODS
    void Start()
    {

        StartCoroutine(LoadNextSceneAfter(loadingDelay));
    }
    #endregion //MONOBEHAVIOUR_METHODS

#if UNITY_ANDROID

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            MobileNativeDialog dialog = new MobileNativeDialog("CONFIRM EXIT", "Do you want to quit the game?");
            //   Application.Quit(); 
            Time.timeScale = 0;

            dialog.OnComplete += OnDialogClose;
        }
    }
#endif
    private void OnDialogClose(MNDialogResult result)
    {
        switch (result)
        {
            case MNDialogResult.YES:
                //Debug.Log("Yes button pressed"); 
                Application.Quit();
                break;
            case MNDialogResult.NO:
                Debug.Log("No button pressed");
                Time.timeScale = 1;
                break;
        }
    }





    #region PRIVATE_METHODS
    private IEnumerator LoadNextSceneAfter(float seconds)
    {
        yield return new WaitForSeconds(seconds);

#if (UNITY_5_2 || UNITY_5_1 || UNITY_5_0)
        Application.LoadLevel(Application.loadedLevel+1);
#else
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + 1);
#endif
    }
    #endregion //PRIVATE_METHODS
}


